angular.module('app.directives', ['ngMessages'])

.directive('telappinput', function() {
	return {
		restrict: "AE",
		replace: true,
		transclude: true,
		scope: {
			indata: '=outdata',
			inmodel: '=outmodel',
			inerror: '=outerror',
			inmsg: '=outmsg',
			inplaceholder: '=outplaceholder'
		},
		templateUrl: 'app/telappinput.html'
	};
});