/**
*  Module telapp
*
* prototype app
*/
var telapp = angular.module('telapp', ['pascalprecht.translate', 'ngMessages', 'app.controllers', 'app.services', 'app.directives']);

telapp.config(['$translateProvider', function ($translateProvider) {
	$translateProvider.translations('en', {
		'SIGN_IN': 'SIGN IN',
		'SUBMIT_BTN': 'Sign in',
		'INFO_TOP': 'Please enter your information below',
		'INFO_BOT': 'All fields are mandatory',
		'PLACEHOLDER0': 'Customers account number',
		'PLACEHOLDER1': 'Customers date of birth',
		'PLACEHOLDER2': 'Customers post code',
		'PLACEHOLDER3': 'Customers last name',
		'PLACEHOLDER4': 'Customers mobile number'
	});

	$translateProvider.translations('de', {
		'SIGN_IN': 'ANMELDEN',
		'SUBMIT_BTN': 'Anmelden',
		'INFO_TOP': 'Bitte geben Sie Ihre Informationen unter',
		'INFO_BOT': 'Alle Felder sind Pflichtfelder',
		'PLACEHOLDER0': 'Kunden -Kontonummer',
		'PLACEHOLDER1': 'Kunden, Geburtsdatum',
		'PLACEHOLDER2': 'Kunden, Postleitzahl',
		'PLACEHOLDER3': 'Kunden, Nachname',
		'PLACEHOLDER4': 'Kunden, Handynummer' 
	});

	$translateProvider.translations('fr', {
		'SIGN_IN': 'SE CONNECTER',
		'SUBMIT_BTN': 'Se connecter',
		'INFO_TOP': "S'il vous plaît entrer vos informations ci-dessous",
		'INFO_BOT': 'Tous les champs sont obligatoires',
		'PLACEHOLDER0': 'Les clients numéro de compte',
		'PLACEHOLDER1': 'Les clients Date de naissance',
		'PLACEHOLDER2': 'Les clients Code postal',
		'PLACEHOLDER3': 'Les clients nom',
		'PLACEHOLDER4': 'Les clients numéro de téléphone mobile'
	});

	$translateProvider.translations('es', {
		'SIGN_IN': 'REGISTRARSE',
		'SUBMIT_BTN': 'Registrarse',
		'INFO_TOP': 'Por favor, introduzca sus datos a continuación',
		'INFO_BOT': 'Todos los campos son obligatorios',
		'PLACEHOLDER0': 'Clientes número de teléfono móvil',
		'PLACEHOLDER1': 'Los clientes Fecha de nacimiento',
		'PLACEHOLDER2': 'Clientes Código Postal',
		'PLACEHOLDER3': 'Los clientes apellido',
		'PLACEHOLDER4': 'Clientes número de teléfono móvil'
	});

	$translateProvider.preferredLanguage('es');
}]);
