angular.module('app.services', [])

.factory('DataService', ['$http', function($http) {
	var DataServiceInst = {
		storedData: {},
		submitStatus: false,
		getAll: function() {
			return $http({
				method: 'POST',
				url: 'https://private-77a70-testdummyapi.apiary-mock.com/idv/process'
			}).then(
			function(response) {
				DataServiceInst.storedData = response;
				console.log(response.data.data);
				return response.data.data;
			},
			function(error) {
				console.log('Error finding data');
			});
		},
		sendData: function( obj ) {
			console.log('sending data: ', obj );
			return $http({
				method: 'POST',
				url: 'https://private-77a70-testdummyapi.apiary-mock.com/idv/process',
				data: obj
			}).then(
			function(response) {
				console.log('sent -- now returning');
				return DataServiceInst.submitStatus = true;
			},
			function(error) {
				console.log('not sent -- error: ', error );
				return DataServiceInst.submitStatus = false;
			});
		}
	};
	return DataServiceInst;
}])

.factory('MessageService', ['$http', function($http) {
	var MessageServiceInst = {
		getAll: function() {
			return $http({
				method: 'GET',
				url: 'app/messages.json'
			}).then(
			function(response) {
				return response.data;
			}, 
			function(error) {
				console.log('Error getting messages');
			});
		}
	};
	return MessageServiceInst;
}]);