angular.module('app.controllers', [])

.controller('customerFormController', ['$timeout', '$rootScope', '$scope', '$translate', 'DataService', 'MessageService', function($timeout, $rootScope, $scope, $translate, DataService, MessageService) {
	var inputArr = [
		{inputName:'customer_num', inputType:'text', inputMinlen:4, inputPattern:"[0-9]{4}\\s[0-9]{4}\\s[0-9]{5}\\s[0-9]{4}", inputIcon:'glyphicon-sound-5-1'},
		{inputName:'customer_dob', inputType:'text', inputMinlen:0, inputPattern:"^[0-9]{2}[-]{1}[0-9]{2}[-]{1}[0-9]{4}$", inputIcon:'glyphicon-calendar'},
		{inputName:'customer_pcode', inputType:'text', inputMinlen:0, inputPattern:"[A-Za-z0-9]{3,4}\\s[A-Za-z0-9]{3,4}", inputIcon:'glyphicon-envelope'},
		{inputName:'customer_lname', inputType:'text', inputMinlen:4, inputPattern:"[A-Za-z]+", inputIcon:'glyphicon-user'},
		{inputName:'customer_phone', inputType:'tel', inputMinlen:0, inputPattern:"[0-9-]{7,}", inputIcon:'glyphicon-phone'}
	];

	$scope.errorMessages;
	$scope.allData;
	$scope.fieldArr = [];
	$scope.submitStatus = DataService.submitStatus;

	MessageService.getAll().then(function(response) {
		$scope.errorMessages = response;
	});
	DataService.getAll().then(function(response) {
		$scope.allData = response;

		for(var i=0; i<$scope.allData.length; i++) {
			 if( $scope.allData[i].hasOwnProperty("field") ) {
			 	var textVal = $scope.allData[i].field;
			 	var textEdit = textVal.split('.');
			 	var textRes = textEdit[1];
			 	$scope.allData[i].field = textRes;
			 	$scope.fieldArr[i] = textRes;
			 	console.log($scope.allData[i].field);
			 }
			 angular.extend($scope.allData[i], inputArr[i]);
		};
		console.log($scope.allData);
	});

	$scope.telrockSubmit = function() {
		console.log('telrockSubmit:');
		DataService.sendData( $rootScope.testmodel ).then(
			function(response) {
				$timeout(function() {
					$scope.submitStatus = DataService.submitStatus;
				}, 100).then(function() {
					$timeout(function() {
						DataService.submitStatus = false;
						$scope.submitStatus = DataService.submitStatus;
					}, 2000);
				});
				
			},
			function(error) {
				$scope.submitStatus = DataService.submitStatus;
			}
		);

	};

	$scope.switchLang = function( langString ) {
		console.log( langString );
		$translate.use( langString );
	}

}]);